
$(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 10,
                loop: true,
                margin: 0,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
        responsiveClass:true,
        responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
     1000:{
            items:1,
            nav:false
        },
        1300:{
            items:1,
            nav:true,
            nav:false,
            loop:true
    }}
              });

    $('ul.nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).slideDown(500);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).slideUp(500);
    });
    function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less");
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);

});