import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestCache } from './cache/request-cache.service';
import { CachingInterceptor } from './cache/caching-interceptor.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { SlickCarouselModule } from 'ngx-slick-carousel';

import { HeaderComponent } from './pages/header/header.component';
import { FooterComponent } from './pages/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { CareerComponent } from './pages/career/career.component';
import { ContactComponent } from './pages/contact/contact.component';
import { DesignUiUxComponent } from './pages/design-ui-ux/design-ui-ux.component';
import { DevelopmentComponent } from './pages/development/development.component';
import { DigitalMarketingComponent } from './pages/digital-marketing/digital-marketing.component';
import { MobileDevelopmentComponent } from './pages/mobile-development/mobile-development.component';
import { PartnerDeliveryComponent } from './pages/partner-delivery/partner-delivery.component';
import { PhotographyComponent } from './pages/photography/photography.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { ServicesComponent } from './pages/services/services.component';
import { WebDevelopmentComponent } from './pages/web-development/web-development.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    CareerComponent,
    ContactComponent,
    DesignUiUxComponent,
    DevelopmentComponent,
    DigitalMarketingComponent,
    MobileDevelopmentComponent,
    PartnerDeliveryComponent,
    PhotographyComponent,
    PortfolioComponent,
    ServicesComponent,
    WebDevelopmentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SlickCarouselModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: CachingInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
