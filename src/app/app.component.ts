import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MagnusIdeas';
  offline: boolean;
   onActivate(event) {
	    window.scroll(0,0);
      document.getElementById("close").click(); 
	}

	onNetworkStatusChange() {
  this.offline = !navigator.onLine;
  console.log('offline ' + this.offline);
}
}
