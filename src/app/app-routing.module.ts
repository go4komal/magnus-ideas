import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { CareerComponent } from './pages/career/career.component';
import { ContactComponent } from './pages/contact/contact.component';
import { DesignUiUxComponent } from './pages/design-ui-ux/design-ui-ux.component';
import { DevelopmentComponent } from './pages/development/development.component';
import { DigitalMarketingComponent } from './pages/digital-marketing/digital-marketing.component';
import { MobileDevelopmentComponent } from './pages/mobile-development/mobile-development.component';
import { PartnerDeliveryComponent } from './pages/partner-delivery/partner-delivery.component';
import { PhotographyComponent } from './pages/photography/photography.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { ServicesComponent } from './pages/services/services.component';
import { WebDevelopmentComponent } from './pages/web-development/web-development.component';

const routes: Routes = [

{
	path: '',
	component: HomeComponent,
	data: { title: 'Home' }
},
{
	path: 'home',
	component: HomeComponent,
	data: { title: 'Home' }
},
{
	path: 'about',
	component: AboutComponent,
	data: { title: 'About' }
},
{
	path: 'career',
	component: CareerComponent,
	data: { title: 'Career' }
},
{
	path: 'contact',
	component: ContactComponent,
	data: { title: 'Contact' }
},
{
	path: 'design-ui-ux',
	component: DesignUiUxComponent,
	data: { title: 'Design Ui Ux' }
},
{
	path: 'development',
	component: DevelopmentComponent,
	data: { title: 'Development' }
},
{
	path: 'digital-marketing',
	component: DigitalMarketingComponent,
	data: { title: 'Digital Marketing' }
},
{
	path: 'mobile-development',
	component: MobileDevelopmentComponent,
	data: { title: 'Mobile Development' }
},
{
	path: 'partner-delivery',
	component: PartnerDeliveryComponent,
	data: { title: 'Partner Delivery' }
},
{
	path: 'photography',
	component: PhotographyComponent,
	data: { title: 'Photography' }
},
{
	path: 'portfolio',
	component: PortfolioComponent,
	data: { title: 'Portfolio' }
},
{
	path: 'services',
	component: ServicesComponent,
	data: { title: 'Services' }
},
{
	path: 'web-development',
	component: WebDevelopmentComponent,
	data: { title: 'Web Development' }
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
