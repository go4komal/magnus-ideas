import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignUiUxComponent } from './design-ui-ux.component';

describe('DesignUiUxComponent', () => {
  let component: DesignUiUxComponent;
  let fixture: ComponentFixture<DesignUiUxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignUiUxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignUiUxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
