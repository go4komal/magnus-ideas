import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

 
  slideConfig = {
    "slidesToShow": 1, 
    "slidesToScroll": 1,
    "nextArrow":false,
    "prevArrow":false,
    "autoplay": true,
    "dots":true,
    "infinite": false
  };

  slideConfig1 = {"slidesToShow": 4, "slidesToScroll": 4, 'autoplay': true, 'autoplaySpeed': 3500, 'dots': false, 'infinite': true, 'arrows': true ,'responsive': [{ 'breakpoint': 1600, 'settings': { 'slidesToShow': 3, 'slidesToScroll': 3, } }, { 'breakpoint': 1000, 'settings': { 'slidesToShow': 2, 'slidesToScroll': 2, } }, { 'breakpoint': 600, 'settings': { 'slidesToShow': 1, 'slidesToScroll': 1, } } ]};

  ngOnInit() {
  }

}
