import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  base_url = 'https://magnusideas.mytasks.co.in/api/';

  constructor(private httpClient: HttpClient) { 

  }

   public postContact(formData){


   const formData1 = new FormData();

   formData1.append('name', formData.name);
   formData1.append('email', formData.email);
   formData1.append('city', formData.city);
   formData1.append('message', formData.message);
   formData1.append('phone', formData.phone);

           // let headers = new Headers({ 'Content-Type': 'application/json' });

          //  headers.append('Access-Control-Allow-Origin','*');

          //  let options = new RequestOptions({ headers:headers });

    return this.httpClient.post(this.base_url+'email.php',formData1);
  }
}
