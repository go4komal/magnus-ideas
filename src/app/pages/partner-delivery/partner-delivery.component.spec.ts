import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerDeliveryComponent } from './partner-delivery.component';

describe('PartnerDeliveryComponent', () => {
  let component: PartnerDeliveryComponent;
  let fixture: ComponentFixture<PartnerDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerDeliveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
